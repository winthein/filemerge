using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Web;
using System.Threading;
using System.IO;



namespace WindowsFormsApplication6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public class Translator
        {
            public static IEnumerable<string> Languages
            {
                get
                {
                    Translator.EnsureInitialized();
                    return Translator._languageModeMap.Keys.OrderBy(p => p);
                }
            }
            public TimeSpan TranslationTime
            {
                get;
                private set;
            }
            public Exception Error
            {
                get;
                private set;
            }



            public string Translate
                (string sourceText,
                 string sourceLanguage,
                 string targetLanguage)
            {
                // Initialize
                this.Error = null;


                string translation = string.Empty;

                try
                {
                    // Download translation
                    string url = string.Format("https://translate.googleapis.com/translate_a/single?client=gtx&sl={0}&tl={1}&dt=t&q={2}",
                                                Translator.LanguageEnumToIdentifier(sourceLanguage),
                                                Translator.LanguageEnumToIdentifier(targetLanguage),
                                              WebUtility.UrlEncode(sourceText));
                    string outputFile = Path.GetTempFileName();
                    using (WebClient wc = new WebClient())
                    {
                        wc.Headers.Add("user-age", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");
                        wc.DownloadFile(url, outputFile);
                    }

                    // Get translated text
                    if (File.Exists(outputFile))
                    {

                        // Get phrase collection
                        string text = File.ReadAllText(outputFile);
                        int index = text.IndexOf(string.Format(",,\"{0}\"", Translator.LanguageEnumToIdentifier(sourceLanguage)));
                        if (index == -1)
                        {
                            // Translation of single word
                            int startQuote = text.IndexOf('\"');
                            if (startQuote != -1)
                            {
                                int endQuote = text.IndexOf('\"', startQuote + 1);
                                if (endQuote != -1)
                                {
                                    translation = text.Substring(startQuote + 1, endQuote - startQuote - 1);
                                }
                            }
                        }
                        else
                        {
                            // Translation of phrase
                            text = text.Substring(0, index);
                            text = text.Replace("],[", ",");
                            text = text.Replace("]", string.Empty);
                            text = text.Replace("[", string.Empty);
                            text = text.Replace("\",\"", "\"");

                            // Get translated phrases
                            string[] phrases = text.Split(new[] { '\"' }, StringSplitOptions.RemoveEmptyEntries);
                            for (int i = 0; (i < phrases.Count()); i += 2)
                            {
                                string translatedPhrase = phrases[i];
                                if (translatedPhrase.StartsWith(",,"))
                                {
                                    i--;
                                    continue;
                                }
                                translation += translatedPhrase + "  ";
                            }
                        }

                        // Fix up translation
                        translation = translation.Trim();
                        translation = translation.Replace(" ?", "?");
                        translation = translation.Replace(" !", "!");
                        translation = translation.Replace(" ,", ",");
                        translation = translation.Replace(" .", ".");
                        translation = translation.Replace(" ;", ";");

                        // And translation speech URL
                    }
                }
                catch (Exception ex)
                {
                    this.Error = ex;
                }

                // Return result


                return translation;
            }
            private static string LanguageEnumToIdentifier
               (string language)
            {
                string mode = string.Empty;
                Translator.EnsureInitialized();
                Translator._languageModeMap.TryGetValue(language, out mode);
                return mode;
            }

            /// <summary>
            /// Ensures the translator has been initialized.
            /// </summary>
            private static void EnsureInitialized()
            {
                if (Translator._languageModeMap == null)
                {
                    Translator._languageModeMap = new Dictionary<string, string>();
                    Translator._languageModeMap.Add("Afrikaans", "af");
                    Translator._languageModeMap.Add("Albanian", "sq");
                    Translator._languageModeMap.Add("Arabic", "ar");
                    Translator._languageModeMap.Add("Armenian", "hy");
                    Translator._languageModeMap.Add("Azerbaijani", "az");
                    Translator._languageModeMap.Add("Basque", "eu");
                    Translator._languageModeMap.Add("Belarusian", "be");
                    Translator._languageModeMap.Add("Bengali", "bn");
                    Translator._languageModeMap.Add("Bulgarian", "bg");
                    Translator._languageModeMap.Add("Catalan", "ca");
                    Translator._languageModeMap.Add("Chinese", "zh-CN");
                    Translator._languageModeMap.Add("Croatian", "hr");
                    Translator._languageModeMap.Add("Czech", "cs");
                    Translator._languageModeMap.Add("Danish", "da");
                    Translator._languageModeMap.Add("Dutch", "nl");
                    Translator._languageModeMap.Add("English", "en");
                    Translator._languageModeMap.Add("Esperanto", "eo");
                    Translator._languageModeMap.Add("Estonian", "et");
                    Translator._languageModeMap.Add("Filipino", "tl");
                    Translator._languageModeMap.Add("Finnish", "fi");
                    Translator._languageModeMap.Add("French", "fr");
                    Translator._languageModeMap.Add("Galician", "gl");
                    Translator._languageModeMap.Add("German", "de");
                    Translator._languageModeMap.Add("Georgian", "ka");
                    Translator._languageModeMap.Add("Greek", "el");
                    Translator._languageModeMap.Add("Haitian Creole", "ht");
                    Translator._languageModeMap.Add("Hebrew", "iw");
                    Translator._languageModeMap.Add("Hindi", "hi");
                    Translator._languageModeMap.Add("Hungarian", "hu");
                    Translator._languageModeMap.Add("Icelandic", "is");
                    Translator._languageModeMap.Add("Indonesian", "id");
                    Translator._languageModeMap.Add("Irish", "ga");
                    Translator._languageModeMap.Add("Italian", "it");
                    Translator._languageModeMap.Add("Japanese", "ja");
                    Translator._languageModeMap.Add("Korean", "ko");
                    Translator._languageModeMap.Add("Lao", "lo");
                    Translator._languageModeMap.Add("Latin", "la");
                    Translator._languageModeMap.Add("Latvian", "lv");
                    Translator._languageModeMap.Add("Lithuanian", "lt");
                    Translator._languageModeMap.Add("Macedonian", "mk");
                    Translator._languageModeMap.Add("Malay", "ms");
                    Translator._languageModeMap.Add("Maltese", "mt");
                    Translator._languageModeMap.Add("Norwegian", "no");
                    Translator._languageModeMap.Add("Persian", "fa");
                    Translator._languageModeMap.Add("Polish", "pl");
                    Translator._languageModeMap.Add("Portuguese", "pt");
                    Translator._languageModeMap.Add("Romanian", "ro");
                    Translator._languageModeMap.Add("Russian", "ru");
                    Translator._languageModeMap.Add("Serbian", "sr");
                    Translator._languageModeMap.Add("Slovak", "sk");
                    Translator._languageModeMap.Add("Slovenian", "sl");
                    Translator._languageModeMap.Add("Spanish", "es");
                    Translator._languageModeMap.Add("Swahili", "sw");
                    Translator._languageModeMap.Add("Swedish", "sv");
                    Translator._languageModeMap.Add("Tamil", "ta");
                    Translator._languageModeMap.Add("Telugu", "te");
                    Translator._languageModeMap.Add("Thai", "th");
                    Translator._languageModeMap.Add("Turkish", "tr");
                    Translator._languageModeMap.Add("Ukrainian", "uk");
                    Translator._languageModeMap.Add("Urdu", "ur");
                    Translator._languageModeMap.Add("Vietnamese", "vi");
                    Translator._languageModeMap.Add("Welsh", "cy");
                    Translator._languageModeMap.Add("Yiddish", "yi");
                    Translator._languageModeMap.Add("Cambodia", "km");
                    Translator._languageModeMap.Add("Myanmar", "my");

                }
            }
            private static Dictionary<string, string> _languageModeMap;

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void myan_TextChanged(object sender, EventArgs e)
        {

        }

        private void eng_TextChanged(object sender, EventArgs e)
        {



        }

        private void ch_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Translator t = new Translator();
            this.myan.Text = string.Empty;
            this.myan.Update();
            this.ch.Text = string.Empty;
            this.ch.Update();


            try
            {

                this.Cursor = Cursors.WaitCursor;
                this.myan.Text = t.Translate(this.eng.Text.Trim(), "English", "Myanmar");
                this.ch.Text = t.Translate(this.eng.Text.Trim(), "English", "Chinese");


                if (t.Error == null)
                {
                    this.myan.Update();
                    this.ch.Update();


                }
                else
                {
                    MessageBox.Show(t.Error.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {

                this.Cursor = Cursors.Default;

            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

            eng.Text = "";
            myan.Text = "";
            ch.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }


}

